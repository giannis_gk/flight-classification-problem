import pandas as pd
import numpy as np
import time
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.neural_network import MLPClassifier

start_time = time.time()

df_train = pd.read_csv('train.csv')
df_test = pd.read_csv('test.csv')
y_train = df_train['PAX']

lod_train = df_train['LongitudeDeparture']
lad_train = df_train['LatitudeDeparture']
loa_train = df_train['LongitudeArrival']
laa_train = df_train['LatitudeArrival']
new_lo_train = loa_train - lod_train
new_la_train = laa_train - lad_train
new_distance_train = np.hypot(new_lo_train, new_la_train)
df_train.loc[:,'NewDistance'] = pd.Series(new_distance_train, index=df_train.index)

lod_test = df_test['LongitudeDeparture']
lad_test = df_test['LatitudeDeparture']
loa_test = df_test['LongitudeArrival']
laa_test = df_test['LatitudeArrival']
new_lo_test = loa_test - lod_test
new_la_test = laa_test - lad_test
new_distance_test = np.hypot(new_lo_test, new_la_test)
df_test.loc[:,'NewDistance'] = pd.Series(new_distance_test, index=df_test.index)

DepartureTR = pd.get_dummies(df_train['Departure'])
CityDepartureTR = pd.get_dummies(df_train['CityDeparture'])
ArrivalTR = pd.get_dummies(df_train['Arrival'])
CityArrivalTR = pd.get_dummies(df_train['CityArrival'])
df_train = pd.concat([df_train,DepartureTR,CityDepartureTR,ArrivalTR,CityArrivalTR], axis=1)

DepartureTS = pd.get_dummies(df_test['Departure'])
CityDepartureTS = pd.get_dummies(df_test['CityDeparture'])
ArrivalTS = pd.get_dummies(df_test['Arrival'])
CityArrivalTS = pd.get_dummies(df_test['CityArrival'])
df_test = pd.concat([df_test,DepartureTS,CityDepartureTS,ArrivalTS,CityArrivalTS], axis=1)

df_train.drop(df_train.columns[[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]], axis=1, inplace=True)
df_test.drop(df_test.columns[[1, 2, 3, 4, 5, 6, 7, 8, 9,10]], axis=1, inplace= True)

df_train = np.array(df_train.iloc[:,0:].values)

enc = OneHotEncoder(sparse=False)
X_train = enc.fit_transform(df_train)
X_test = enc.transform(df_test)

scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)

mlp = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(5,), random_state=1)
mlp.fit(X_train, y_train)
y_pred = mlp.predict(X_test)

import csv
with open('y_pred.csv', 'w', newline='') as csvfile:
	writer = csv.writer(csvfile, delimiter=',')
	writer.writerow(['Id', 'Label'])
	for i in range(y_pred.shape[0]):
		writer.writerow([i, y_pred[i]])

print('Time: ', time.time() - start_time)
f1_score(y_test, y_pred, average='micro')
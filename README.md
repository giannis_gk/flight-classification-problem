# Flight classification problem

This is a project for the course "Data Science & Web Mining".
It is a classification problem. There is a set of data consisting of a few thousand flights, each flight being described by a set of variables. Each flight has also a variable related to the number of passengers on the flight.
For some flights the value of the variable is known, while fo others it is not. 
The goal of the project is to predict the value of the variable for flights for which it is not available.

